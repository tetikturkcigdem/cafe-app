#!/usr/bin/env bash

pullscript="scripts/pull-remote-strings.rb"

if [ ! -e pubspec.yaml ]; then
    echo "Run this script from the root directory of the project."
    exit 1
fi

ruby $pullscript pl lib/i18n/intl_pl.arb
ruby $pullscript en lib/i18n/intl_en.arb
ruby $pullscript es lib/i18n/intl_es.arb
ruby $pullscript pt-br lib/i18n/intl_ptbr.arb

./regen-i18n.sh
./regen-i18n.sh # From some reason, Flutter's tooling requires to call it 2 times?
