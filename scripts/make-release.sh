#!/usr/bin/env sh

SRCFILE="/tmp/flutter_version.dart"
DSTFILE="lib/flutter_version.dart"

set -euo pipefail

echo "import 'dart:core';" > $SRCFILE
echo "" >> $SRCFILE
echo "const Map<String, String> version = " >> $SRCFILE
flutter --version --machine >> $SRCFILE
echo ";" >> $SRCFILE

HASHA=`sha1sum $SRCFILE | cut -d' ' -f1`
HASHB=`sha1sum $DSTFILE | cut -d' ' -f1`

if [ ! "$HASHA" == "$HASHB" ]; then
    mv -v $SRCFILE $DSTFILE
else
    echo "flutter_version.dart is up-to-date, not updating"
fi

flutter build appbundle --release

echo ""
echo "Output:"
echo `pwd`/build/app/outputs/bundle/release/app.aab
