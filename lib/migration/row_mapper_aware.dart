import 'package:cafe_app/migration/map_aware.dart';

mixin RowMapperAware<T> implements MapAware {
  dynamic emptyInstance();
}
