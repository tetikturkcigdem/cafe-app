mixin MapAware {
  void applyFromMap(dynamic mapObject);
}
