import 'package:cafe_app/migration/migration_methods.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/ads.dart' as Ads;
import 'package:flutter/cupertino.dart';

class ImportBackupPage extends StatefulWidget {
  @override
  _ImportBackupPageState createState() => _ImportBackupPageState();
}

// TODO: make a common abstraction for import/export backup pages
// and just parametrize this abstraction, instead of having duplicate code
// that is very similar.
class _ImportBackupPageState extends State<ImportBackupPage> {
  //bool _nextEnabled = false;
  int _currentMigrationMethod = -1;

  @override
  void initState() {
    super.initState();
    initMigrationMethods(this.setState, MigrationMode.Import);

    setState(() {
      //this._nextEnabled = false;
    });
  }

  List<AbstractPropertyWidget> _buildSignInSections() {
    return [
      LinePropertyWidget.from("Select your source device:", bold: false, center: false),
      ActionSheetPropertyWidget<MigrationMethod>.from(
        this.context,
        "Method:",
        kMigrationMethods,
        message: "Select your source device:",
        stateUpdate: (int idx) =>
          setState(() {
            if(idx != this._currentMigrationMethod) {
              this._currentMigrationMethod = idx;
              //this._nextEnabled = false;
              this._initMigrationMethod(idx, this.context);
            }
          }),
        currentObject: this._currentMigrationMethod),
    ];
  }

  void _initMigrationMethod(int idx, BuildContext context) {
    if(idx >= 0 && idx < kMigrationMethods.length) {
      kMigrationMethods[idx].onShowInit(context);
    }
  }

  List<AbstractPropertyWidget> _buildMigrationSpecificSection() {
    var idx = this._currentMigrationMethod;
    if(idx >= 0 && idx < kMigrationMethods.length) {
      var controlPanelWidget =
        kMigrationMethods[idx].controlImportPanelWidget(this.context);

      kMigrationMethods[idx].setNextEnabledUpdateFn((f) {
        setState(() {
          //this._nextEnabled = f;
        });
      });

      return [
        IdentityPropertyWidget(controlPanelWidget, center: true),
      ].where((w) => w != null).toList();
    } else {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text("Importing from a backup")
      ),
      child: SafeArea(
        child: PropertyListView(
          drawLines: false,
          items: <AbstractPropertyWidget>[] +
            this._buildSignInSections() +
            this._buildMigrationSpecificSection(),
        )),
    );
  }
}
