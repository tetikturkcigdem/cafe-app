import 'package:async/async.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/image_box.dart';
import 'package:cafe_app/property_list_view.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/utils/google_http_client.dart';
import 'package:cafe_app/dialogs.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:googleapis/drive/v3.dart' as gdrive;
import 'package:googleapis/drive/v3.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart' as pp;
import 'dart:io' as io;

class ImportGoogleDrivePage extends StatefulWidget {
  final GoogleSignInAccount acc;
  final Map<String, String> authHeaders;

  ImportGoogleDrivePage({this.acc, this.authHeaders});

  @override
  _ImportGoogleDrivePageState createState() => _ImportGoogleDrivePageState();
}

class _ImportGoogleDrivePageState extends State<ImportGoogleDrivePage> {
  GoogleHttpClient _http;
  bool autoPopCancelled = false;
  bool listReady;
  bool listError;
  bool flagStarted;
  bool flagFailed;
  bool flagSucceeded = false;
  List<gdrive.File> fileList;
  var df = DateFormat('yyyy-MM-dd');
  var nf = NumberFormat();

  @override
  void initState() {
    super.initState();
    this._http = GoogleHttpClient(this.widget.authHeaders);

    setState(() {
      this.listReady = false;
      this.listError = false;
      this.flagStarted = false;
      this.flagSucceeded = false;
      this.flagFailed = false;
    });

    Future.delayed(Duration(seconds: 1)).then((r) {
    }).then((r) {
      this.listBackupItems().then((bool r) {
        setState(() {
          this.listReady = true;
          this.listError = !r;
        });
      });
    });
  }

  Future<List<gdrive.File>> getDriveContents() async {
    try {
      return (await gdrive.DriveApi(http()).files.list(corpora: 'user', orderBy: 'createdTime desc', $fields: '*')).files;
    } catch(e) {
      print("API error during list: ${e.toString()}");
      return null;
    }
  }

  Future<bool> listBackupItems() async {
    this.fileList = await this.getDriveContents();
    return this.fileList != null;
  }

  Future<void> backupListClicked(String fileId, String fileName, int fileSize) async {
    UserDialogs.alertYesNoDialog(this.context,
        title: "Caution!",
        message: "Importing this file will replace all your current townships and players. This cannot be undone.\n\n$fileName\n\nDo you want to proceed?",
        yesHandler: () { startImportOfListEntry(fileId, fileSize); },
        noHandler: () { },
        yesLabel: "Yes, import",
        noLabel: i18n(this.context).globDoNothing);
  }

  Future<void> startImportOfListEntry(String fileId, int fileSize) async {
    io.Directory tempDir;

    print("importStart");
    this.importStart();

    print("entering try");
    try {
      var tempDirRoot = await pp.getTemporaryDirectory();
      tempDir = await tempDirRoot.createTemp("mycafe-import-");

      var maybeFullPath = await this.downloadFileToDir(tempDir, fileId);
      if(!maybeFullPath.isValue) {
        return this.importFailed();
      }

      var fullPath = maybeFullPath.asValue.value;
      // Get error string from this function
      if(!(await DatabaseHelper().importFromCSV(fullPath, tempDir))) {
        return this.importFailed();
      }
    } catch(e) {
      print("Error during import: $e");
    } finally {
      if(tempDir != null) {
        // Clean up temp dir
        tempDir.delete(recursive: true);
      }
    }

    this.importSucceeded();
  }

  Future<Result<String>> downloadMedia(io.Directory dir, String fileId, Media media) async {
    var destPath = join(dir.path, fileId.hashCode.toString());
    io.File fw = io.File(destPath);
    fw.createSync(recursive: true);
    var sink = fw.openWrite();

    try {
      await sink.addStream(media.stream);
      return Result.value(destPath);
    } catch(e) {
      print("Error in downloadMedia: ${e.toString()}");
      return Result.error(e);
    } finally {
      sink.flush();
      sink.close();
    }
  }

  Future<Result<String>> downloadFileToDir(io.Directory dir, String fileId) async {
    var opts = PartialDownloadOptions(ByteRange(0, -1));

    return DriveApi(this.http())
      .files
      .get(fileId, downloadOptions: opts)
      .then((dynamic media) =>
        this.downloadMedia(dir, fileId, media));
  }

  void importStart() {
    setState(() {
      this.flagStarted = true;
      this.flagFailed = false;
      this.flagSucceeded = false;
    });
  }

  void importFailed() {
    setState(() {
      this.flagFailed = true;
    });

    this.autoPop(false);
  }

  void importSucceeded() {
    print("Importing finished");
    setState(() {
      this.flagSucceeded = true;
    });

    this.autoPop(true);
  }

  void autoPop(bool backToTownshipListFlag) {
    Future.delayed(Duration(seconds: 3), () {
      if(!this.autoPopCancelled)
        Navigator.of(this.context).pop(backToTownshipListFlag);
    });
  }

  @override
  Widget build(BuildContext context) {
    var navBar = CupertinoNavigationBar(
      middle: Text("Import from Google Drive"),
    );

    return CupertinoPageScaffold(
      navigationBar: navBar,
      child: SafeArea(
        child: this.buildPage()
      )
    );
  }

  Widget buildWorkingIndicator(String msg) {
    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [CupertinoActivityIndicator()],
    );

    var _paddedCaption = Padding(
      padding: EdgeInsets.all(16),
      child: Text(msg));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    return _padded;
  }

  Widget buildFinalResultScreen(bool result, String msg) {
    Widget imageBox;

    if(result) {
      imageBox = ImageBox(imagePath: "assets/succeeded.png", width: 64, height: 64);
    } else {
      imageBox = ImageBox(imagePath: "assets/failed.png", width: 64, height: 64);
    }

    var _centeredRow = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [imageBox],
    );

    var _paddedCaption = Padding(
      padding: EdgeInsets.all(16),
      child: Text(msg));

    var _centeredCol = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_centeredRow, _paddedCaption],
    );

    var _padded = Padding(
      padding: EdgeInsets.all(8),
      child: _centeredCol,
    );

    var _gestureDetector = GestureDetector(
      child: _padded,
      onTap: () {
        this.autoPopCancelled = true;
        Navigator.of(this.context).pop(result);
      }
    );

    return _gestureDetector;
  }

  Widget buildFileListScreen() {
    return PropertyListView(
      drawLines: true,
      items: <AbstractPropertyWidget>[
        LinePropertyWidget.from("Select your backup.")
      ] + this.fileList.map((gdrive.File f) {
        if(f.mimeType == 'application/vnd.google-apps.folder' || !f.name.endsWith(".zip")) {
          return null;
        }

        int fileSize = int.parse(f.size);
        return TextPropertyWidget.from(f.name, nf.format(fileSize) + " bytes",
          onTapped: (_) => backupListClicked(f.id, f.name, fileSize));
      }).where((r) => r != null).toList()
    );
  }

  Widget buildPage() {
    if(this.flagStarted) {
      if(this.flagSucceeded) {
        return this.buildFinalResultScreen(true, "Import OK!");
      } else if(this.flagFailed) {
        return this.buildFinalResultScreen(false, "There was an error.\n\nPlease try again later.");
      } else {
        return this.buildWorkingIndicator(i18n(this.context).globLoading);
      }
    } else if(this.listReady) {
      if(this.listError) {
        return this.buildFinalResultScreen(false, "There was an error.\n\nPlease try again later.");
      } else {
        return this.buildFileListScreen();
      }
    } else {
      return this.buildWorkingIndicator(i18n(this.context).globLoading);
    }
  }

  GoogleHttpClient http() => this._http;
}
