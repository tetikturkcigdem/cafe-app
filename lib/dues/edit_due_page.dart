import 'package:flutter/cupertino.dart';

class EditDuePage extends StatefulWidget {
  final int collectionId;

  EditDuePage({@required this.collectionId});

  @override
  _EditDuePageState createState() => _EditDuePageState();
}

class _EditDuePageState extends State<EditDuePage> {
  Widget buildBody() {
    return Text("OK, collectionId is ${this.widget.collectionId}.");
  }

  @override
  Widget build(BuildContext context) {
    var navBar = CupertinoNavigationBar(
      middle: Text("Edytuj zrzutkę"),
    );

    return CupertinoPageScaffold(
      navigationBar: navBar,
      child: SafeArea(
        child: this.buildBody(),
      ),
    );
  }
}
