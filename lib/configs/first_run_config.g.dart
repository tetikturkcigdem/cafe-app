// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'first_run_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FirstRunConfig _$FirstRunConfigFromJson(Map<String, dynamic> json) {
  return FirstRunConfig(json['firstRunDate'] == null
      ? null
      : DateTime.parse(json['firstRunDate'] as String));
}

Map<String, dynamic> _$FirstRunConfigToJson(FirstRunConfig instance) =>
    <String, dynamic>{'firstRunDate': instance.firstRunDate?.toIso8601String()};
