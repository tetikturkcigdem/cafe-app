// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'vote_dialog_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoteDialogConfig _$VoteDialogConfigFromJson(Map<String, dynamic> json) {
  return VoteDialogConfig(
      json['lastDisplayDate'] == null
          ? null
          : DateTime.parse(json['lastDisplayDate'] as String),
      json['neverDisplay'] as bool)
    ..lastVersionCode = json['lastVersionCode'] as int;
}

Map<String, dynamic> _$VoteDialogConfigToJson(VoteDialogConfig instance) =>
    <String, dynamic>{
      'lastDisplayDate': instance.lastDisplayDate?.toIso8601String(),
      'neverDisplay': instance.neverDisplay,
      'lastVersionCode': instance.lastVersionCode
    };
