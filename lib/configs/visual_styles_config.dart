import 'package:cafe_app/configs/json_aware.dart';
import 'package:flutter/foundation.dart';

class VisualStylesConfig implements JsonAware {
  VisualStylesConfig({@required this.enableDarkMode});

  static const String ENABLE_DARK_MODE = "enableDarkMode";

  bool enableDarkMode;

  @override
  Map<String, dynamic> toJson() {
    var newMap = Map<String, dynamic>();
    newMap.addAll(JsonAware.writeOptionBool(ENABLE_DARK_MODE, this.enableDarkMode));
    return newMap;
  }

  factory VisualStylesConfig.fromJson(Map<String, dynamic> src) {
    return VisualStylesConfig(
      enableDarkMode: JsonAware.readOptionBool(src, ENABLE_DARK_MODE, false),
    );
  }
}
