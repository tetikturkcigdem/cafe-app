/*import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ChartData {
  String name;
  int points;

  ChartData(this.name, this.points);
}

typedef ChartDataCallback = Future<List<ChartData>> Function();

class ChartWidget extends StatefulWidget {
  ChartWidget({@required this.loadData, @required this.title, this.subtitle});

  final String title;
  final String subtitle;
  final ChartDataCallback loadData;

  @override
  _ChartWidgetState createState() => _ChartWidgetState();
}

class _ChartWidgetState extends State<ChartWidget> {
  List<ChartData> data;

  @override
  void initState() {
    super.initState();
    this.widget.loadData().then((r) => setState(() { this.data = r; }));
  }

  List<charts.Series<ChartData, String>> _createSeries() {
    return [
      charts.Series<ChartData, String>(
        id: "chart",
        domainFn: (cd, _) => cd.name,
        measureFn: (cd, _) => cd.points,
        data: this.data,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.title)
      ),
      body: charts.BarChart(
        _createSeries(),
        animate: true,
        vertical: false,
      ),
    );
  }
}*/
