import 'package:cafe_app/strings.dart';
import 'package:flutter/cupertino.dart';

Widget dismissRemoveContainer(BuildContext context) {
  return Container(
    color: CupertinoColors.destructiveRed,
    alignment: Alignment.centerRight,
    child: Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 16, 0),
      child: Row(
        children: [
          Expanded(child: Text("")),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 8, 0),
            child: Text(StringLocalizations.of(context).globRemove,
              style: TextStyle(color: CupertinoColors.white))),
          Icon(CupertinoIcons.delete, color: CupertinoColors.white),
        ])
    )
  );
}
