import 'package:flutter/cupertino.dart';

class PageUtils {
  static Future<T> push<T extends Object>(BuildContext context, Widget page) {
    var route = CupertinoPageRoute<T>(builder: (context) => page);
    return Navigator.push<T>(context, route);
  }
}