import 'package:cafe_app/configs/config_loader.dart';
import 'package:cafe_app/configs/visual_styles_config.dart';
import 'package:cafe_app/configs/vote_dialog_config.dart';
import 'package:cafe_app/configs/first_run_config.dart';

class GlobalConfig {
  VoteDialogConfig _voteDialogConfig;
  FirstRunConfig _firstRunConfig;
  VisualStylesConfig _visualStylesConfig;

  bool get enableDarkMode {
    print("reading enableDarkMode from visualStylesConfig object");
    print(_visualStylesConfig);
    return _visualStylesConfig.enableDarkMode;
  }

  int get runningInDays =>
    DateTime.now().difference(this._firstRunConfig.firstRunDate).inDays;

  DateTime get lastVoteDisplay =>
    this._voteDialogConfig.lastDisplayDate;

  int get lastVersionCode =>
    this._voteDialogConfig.lastVersionCode;

  // How many days ago the Vote dialog was shown?
  int get lastVoteDisplayInDays =>
      DateTime.now().difference(this._voteDialogConfig.lastDisplayDate).inDays;

  // Don't ever show the Vote dialog.
  bool get neverShowVoteDialog => this._voteDialogConfig.neverDisplay;

  // How many days must pass after first app run to show the Vote dialog?
  int get voteDialogMinimumDaysFromFirstRun => 14;

  // How many days from last display of Vote dialog must pass to display the
  // dialog again?
  int get voteDialogMinimumDaysFromLastDisplay => 45;

  GlobalConfig();

  static Future<GlobalConfig> initAsync() async {
    globalConfigInstance = GlobalConfig();
    await globalConfigInstance.forceLoad();
    return globalConfigInstance;
  }

  static GlobalConfig get() => globalConfigInstance;

  Future<void> forceLoad() async {
    print("loading configs");
    await this._loadFirstRunConfig();
    await this._loadVoteDialogConfig();
    await this._loadVisualStylesConfig();
    print("config load ok");
  }

  Future<void> _loadFirstRunConfig() async {
    var firstRunConfig = await ConfigLoader.getFirstRunConfig();

    if(firstRunConfig.isValue) {
      this._firstRunConfig = firstRunConfig.asValue.value;
    } else {
      this._firstRunConfig = FirstRunConfig(DateTime.now());
      this._persistFirstRunConfig();
    }
  }

  Future<void> _loadVoteDialogConfig() async {
    var voteDialogConfig = await ConfigLoader.getVoteDialogConfig();
    if(voteDialogConfig.isValue) {
      this._voteDialogConfig = voteDialogConfig.asValue.value;
    } else {
      this._voteDialogConfig = VoteDialogConfig(DateTime.now(), false);
      this._persistVoteDialogConfig();
    }
  }

  Future<void> _loadVisualStylesConfig() async {
    print("loading visual styles");
    var visualStylesConfig = await ConfigLoader.getVisualStylesConfig();
    if(visualStylesConfig.isValue) {
      this._visualStylesConfig = visualStylesConfig.asValue.value;
    } else {
      this._visualStylesConfig = VisualStylesConfig(enableDarkMode: false);
      this._persistVisualStylesConfig();
    }
  }

  Future<bool> _persistFirstRunConfig() async =>
    ConfigLoader.saveFirstRunConfig(this._firstRunConfig);

  Future<bool> _persistVoteDialogConfig() async =>
    ConfigLoader.saveVoteDialogConfig(this._voteDialogConfig);

  Future<bool> _persistVisualStylesConfig() async =>
    ConfigLoader.saveVisualStylesConfig(this._visualStylesConfig);

  set neverShowVoteDialog(bool flag) {
    this._voteDialogConfig.neverDisplay = flag;
    this._persistVoteDialogConfig();
  }

  set lastVoteDisplay(DateTime when) {
    this._voteDialogConfig.lastDisplayDate = when;
    this._persistVoteDialogConfig();
  }

  set lastVersionCode(int code) {
    this._voteDialogConfig.lastVersionCode = code;
    this._persistVoteDialogConfig();
  }

  set enableDarkMode(bool enable) {
    this._visualStylesConfig.enableDarkMode = enable;
    this._persistVisualStylesConfig();
  }
}

GlobalConfig globalConfigInstance;
