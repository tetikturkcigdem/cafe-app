import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;
import 'dart:io';

import 'package:package_info/package_info.dart';
import 'package:async/async.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart' as pp;
import 'package:csv/csv.dart';
import 'package:archive/archive.dart';
import 'package:archive/archive_io.dart';
import 'package:sprintf/sprintf.dart';
import 'package:tuple/tuple.dart';

import 'package:cafe_app/township.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/festival.dart';
import 'package:cafe_app/score.dart';
import 'package:cafe_app/selection.dart';
import 'package:cafe_app/migration/row_mapper.dart';

typedef ImportObjectCallback = void Function(Map<String, Object> data);

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  static Database _db;

  static const String DB_FILENAME = "mycafe.db";
  static const String TABLE_TOWNSHIPS = "restaurants";
  static const String TABLE_USERS = "users";
  static const String TABLE_FESTIVALS = "festivals";
  static const String TABLE_SCORE = "score";
  static const String TABLE_SELECTION = "selection";
  static const String TABLE_METADATA = "configs";
  static const String TABLE_DUES = "dues";
  static const String TABLE_PAYMENTS = "payments";

  DatabaseHelper.internal();

  Future<Database> get db async {
    if(DatabaseHelper._db == null) {
      return await initIfNeeded() ? DatabaseHelper._db : null;
    } else {
      return DatabaseHelper._db;
    }
  }

  static Future<void> _addVersion2SelectionTable(Database db) async {
    print("DatabaseHelper: creating $TABLE_SELECTION");
    await db.execute("CREATE TABLE $TABLE_SELECTION (id integer primary key, rid int, fid int, sid int, flag int)");
  }

  static Future<void> _addVersion3TasksInfo(Database db) async {
    print("DatabaseHelper: adding information about tasks");
    await db.execute("ALTER TABLE $TABLE_SCORE add column tasks int default 0");
  }

  static Future<void> _addVersion3DistrInfo(Database db) async {
    await db.execute("ALTER TABLE $TABLE_FESTIVALS add column tasks int default 0");
    await db.execute("ALTER TABLE $TABLE_FESTIVALS add column method int default 1");
  }

  static Future<void> _addVersion4Metadata(Database db) async {
    await db.execute("CREATE TABLE $TABLE_METADATA (id integer primary key, confid int, conftext text)");
  }

  static Future<void> _addVersion5Dues(Database db) async {
    await db.execute("CREATE TABLE $TABLE_DUES (id integer primary key, " +
      "rid int, " +
      "goal int, " +
      "dateMSFromEpoch int," +
      "type int)");

    await db.execute("CREATE TABLE $TABLE_PAYMENTS (id integer primary key, " +
      "uid int, coins int, diamonds int, dateMSFromEpoch int)");
  }

  static Future<void> _upgradeFromVersion1(Database db) async {
    print("DatabaseHelper: upgradeFromVersion1");
    await _addVersion2SelectionTable(db);
  }

  static Future<void> _upgradeFromVersion2(Database db) async {
    print("DatabaseHelper: upgradeFromVersion2");
    await _addVersion3TasksInfo(db);
    await _addVersion3DistrInfo(db);
  }

  static Future<void> _upgradeFromVersion3(Database db) async {
    print("DatabaseHelper: upgradeFromVersion3");
    await _addVersion4Metadata(db);
  }

  static Future<void> _upgradeFromVersion4(Database db) async {
    print("DatabaseHelper: upgradeFromVersion4");
    await _addVersion5Dues(db);
  }

  static Future<bool> checkForBadUpgradeBugExistence() async {
    // The bug is:
    //
    // Database in current installation is in version 4, but TABLE_METADATA
    // doesn't exist.

    try {
      var _ = await DatabaseHelper._db.rawQuery(
        "SELECT count(*) as cnt from $TABLE_METADATA");
      return false;
    } on DatabaseException catch(_) {
      // TABLE_METADATA database doesn't exist in current installation,
      // so the bug is active.
      return true;
    }
  }

  // Increase this version on each change of the database schema.
  static const int DB_VERSION = 5;
  static Future<bool> initIfNeeded() async {
    var dbPath = await getDatabasesPath();
    var path = join(dbPath, DB_FILENAME);

    print("DatabaseHelper.initIfNeeded");

    DatabaseHelper._db = await openDatabase(path,
      version: DB_VERSION,
      onCreate: (Database db, int version) async {
        print("DatabaseHelper: creating $TABLE_TOWNSHIPS");
        await db.execute("CREATE TABLE $TABLE_TOWNSHIPS (id integer primary key, name text)");
        print("DatabaseHelper: creating $TABLE_USERS");
        await db.execute("CREATE TABLE $TABLE_USERS (id integer primary key, rid integer, name text, disabled int)");
        print("DatabaseHelper: creating $TABLE_FESTIVALS");
        await db.execute("CREATE TABLE $TABLE_FESTIVALS (id integer primary key, rid integer, name text, date int, yellow int, red int, blue int)");
        print("DatabaseHelper: creating $TABLE_SCORE");
        await db.execute("CREATE TABLE $TABLE_SCORE (id integer primary key, rid int, fid int, uid int, yellow int)");
        await _addVersion2SelectionTable(db);
        await _addVersion3TasksInfo(db);
        await _addVersion3DistrInfo(db);
        await _addVersion4Metadata(db);
        await _upgradeFromVersion4(db);
        print("DatabaseHelper: done");
      },
      onUpgrade: (Database db, int oldVer, int newVer) async {
        print("Updating database, oldVer=$oldVer, newVer=$newVer...");

        assert(newVer == DB_VERSION);
        switch(oldVer) {
          case 1:
            if(newVer >= 2)
              await _upgradeFromVersion1(db);

            if(newVer >= 3)
              await _upgradeFromVersion2(db);

            if(newVer >= 4)
              await _upgradeFromVersion3(db);

            if(newVer >= 5)
              await _upgradeFromVersion4(db);

            break;

          case 2:
            if(newVer >= 3)
              await _upgradeFromVersion2(db);

            if(newVer >= 4)
              await _upgradeFromVersion3(db);

            if(newVer >= 5)
              await _upgradeFromVersion4(db);

            break;

          case 3:
            if(newVer >= 4)
              await _upgradeFromVersion3(db);

            if(newVer >= 5)
              await _upgradeFromVersion4(db);

            break;

          case 4:
            if(newVer >= 5)
              await _upgradeFromVersion4(db);

            break;

          case 5:
            // Upgrade from 5 to 5 shouldn't happen.
            break;

          default:
            break;
            // shouldn't happen!
        }
      },
    );

    // There was a bug, when upgrading from version 1 or 2 didn't update
    // the DB properly. This fix should fix old installations.
    if(await DatabaseHelper.checkForBadUpgradeBugExistence()) {
      await DatabaseHelper._addVersion4Metadata(DatabaseHelper._db);
    }

    return true;
  }

  Future<bool> processImportArchive(List<String> filesToProcess, String fullPathToZIP) async {
    var codec = CsvCodec();
    Map<String, bool> filesProcessed =
      filesToProcess.asMap().map((index, e) => MapEntry(e, false));

    try {
      print("processImportArchive(): start");
      var zipFileContent = File(fullPathToZIP).readAsBytesSync();
      var zipDecoder = ZipDecoder().decodeBytes(zipFileContent);

      for(var file in zipDecoder.files) {
        print("processImportArchive(): processing file ${file.name}");
        if(file.name == 'townships.csv') {
          await this.importTownshipsFromCSV_4(file.content, codec);
          filesProcessed[file.name] = true;
        } else if(file.name == 'users.csv') {
          await this.importUsersFromCSV_4(file.content, codec);
          filesProcessed[file.name] = true;
        } else if(file.name == 'festivals.csv') {
          await this.importFestivalsFromCSV_4(file.content, codec);
          filesProcessed[file.name] = true;
        } else if(file.name == 'scores.csv') {
          await this.importScoresFromCSV_4(file.content, codec);
          filesProcessed[file.name] = true;
        } else if(file.name == 'selections.csv') {
          await this.importSelectionsFromCSV_4(file.content, codec);
          filesProcessed[file.name] = true;
        }
      }

      var unprocessedFiles = filesProcessed
          .entries
          .where((MapEntry e) => !e.value)
          .map((e) => e.key)
          .toList();

      if(unprocessedFiles.length > 0) {
        // Some files that were declared were not processed. This means that
        // most probably the archive was tampered, corrupted, there's bug
        // in the code, or something strange has happened.

        print("Error during import: some files were not processed:");
        unprocessedFiles.forEach((f) => print(f));
        return false;
      }
    } catch(e) {
      print("Unhandled exception: $e");
    } finally {

    }

    return true;
  }

  Future<Result<Map<String, Object>>> readMetaJson(String fullPathToZIP) async {
    // This is unfortunate that the 'archive' lib requires to read
    // whole archive to memory before extracting anything from it ;(.
    //
    // Remember to add a new InputStream subclass that supports
    // reading data directly from the file instead of from memory.

    Map<String, Object> jsonData;

    try {
      print("readMetaJson(): start");
      var zipFileContent = File(fullPathToZIP).readAsBytesSync();
      var zipDecoder = ZipDecoder().decodeBytes(zipFileContent);

      for(var file in zipDecoder.files) {
        // Unpack the file to test it unpacks properly. Better to fail
        // unpacking process here, than after removing whole database from
        // the device. If the unpacking process fails here, we can simply
        // show an 'import error' message instead of destroying whole data
        // of the user without the chance of getting it back!

        if(file.name == 'meta.json') {
          List<int> fileContents = file.content;
          if(fileContents.length < 10) {
            // meta.json which is smaller than 10 bytes is most certainly an
            // invalid metadata file. If this is the case, we already know that
            // whole backup archive is corrupted and we can't import anything
            // from it.
            return Result.error("meta.json has invalid size");
          }

          try {
            jsonData = jsonDecode(utf8.decode(fileContents));

            // We don't return immediately because we want to test if rest
            // of the files will unpack properly.
          } catch(e) {
            return Result.error("invalid UTF-8 string in meta.json");
          }
        }
      }
    } catch(e) {
      return Result.error(e);
    }

    return Result.value(jsonData);
  }

  Future<bool> importFromCSV(String fullPathToZIP, io.Directory tempDir) async {
    var metaJson = await this.readMetaJson(fullPathToZIP);
    if(metaJson == null) {
      print("readMetaJson() returned null.");
      return false;
    }

    if(metaJson.isError) {
      print("readMetaJson() returned error: ${metaJson.asError.error.toString()}");
      return false;
    }

    var meta = metaJson.asValue.value;
    if(meta == null) {
      print("readMetaJson() result value was null");
      return false;
    }

    // Find failures -- functor should return `true` if there was any error when
    // processing a file. It should return `false` if there was no such error.
    var failureFound = ['dbVersion', 'appVersion', 'files', 'buildNumber', 'creationDate']
        .any((String fieldName) {
          if(!meta.containsKey(fieldName)) {
            print("readMetaJson() returned an invalid meta.json object: missing '$fieldName' field.");
            return true;
          } else {
            return false;
          }
        });

    if(failureFound)
      return false;

    try {
      var pi = await PackageInfo.fromPlatform();

      var dbVersion = meta['dbVersion'].toString();
      var appVersion = meta['appVersion'].toString();
      var files = meta['files'];
      var buildNumber = meta['buildNumber'].toString();

      print("dbVersion: $dbVersion");
      print("appVersion: $appVersion");
      print("buildNumber: $buildNumber");
      print("files: $files");

      if(int.parse(buildNumber) > int.parse(pi.buildNumber)) {
        print("Can't import data from a future version of backup file!");
        return false;
      }

      print("Removing all townships and all users...");
      await this.removeAllTownships();
      await this.removeAllUsers();

      print("Importing backup data...");

      List<String> filesList = List<String>();

      // Hackity hack!
      (files as dynamic).forEach((f) => filesList.add(f.toString()));

      var result = await this.processImportArchive(filesList, fullPathToZIP);
      print("Import finished with result $result.");
      return result;
    } finally {
      // Caller will delete the zip file along with the whole temporary
      // directory, just make sure nothing is opened.
    }
  }

  Future<Result<Tuple2<Directory, String>>> exportToCSV() async {
    var tempDirRoot = await pp.getTemporaryDirectory();
    io.Directory tempDir;
    var pi = await PackageInfo.fromPlatform();

    try {
      // Create data in CSV format.
      tempDir = await tempDirRoot.createTemp("mycafe-backup-");
      await this.exportTownshipsToCSV_4(join(tempDir.path, 'townships.csv'));
      await this.exportUsersToCSV_4(join(tempDir.path, 'users.csv'));
      await this.exportFestivalsToCSV_4(join(tempDir.path, 'festivals.csv'));
      await this.exportScoresToCSV_4(join(tempDir.path, 'scores.csv'));
      await this.exportSelectionsToCSV_4(join(tempDir.path, 'selections.csv'));
      await this.writeJsonToFile(join(tempDir.path, "meta.json"), {
        "dbVersion": 4,
        "appVersion": pi.version,
        "buildNumber": pi.buildNumber,
        "creationDate": DateTime.now().toString(),
        "files": ['townships.csv', 'users.csv', 'festivals.csv',
          'scores.csv', 'selections.csv']
      });

      var zipEncoder = ZipFileEncoder();
      String fullPath = join(tempDir.path, await _buildArchiveName());

      // Create ZIP file with the data.
      zipEncoder.create(fullPath);
      ['townships.csv', 'users.csv', 'festivals.csv',
        'scores.csv', 'selections.csv', 'meta.json']
          .forEach((String filename) {
        zipEncoder.addFile(io.File(join(tempDir.path, filename)));
      });

      zipEncoder.close();

      // Return temp dir object along with a full path to the ZIP file.
      return Result.value(Tuple2<Directory, String>(tempDir, fullPath));
    } catch(e) {
      // Failure during creation of CSV files or creation of ZIP file.
      return Result.error(e);
    }
  }

  Future<String> _buildArchiveName() async {
    var now = DateTime.now();
    var str = sprintf.call("backup-%04d-%02d-%02d.zip", [
      now.year, now.month, now.day
    ]);
    return str;
  }

  Future<bool> importObjectFromCSV_4(List<int> csvData, CsvCodec codec, ImportObjectCallback callback) async {
    List<List> lst = codec.decoder.convert(utf8.decode(csvData));
    var mapper = RowMapper(lst);

    mapper.eachObject((src) async => callback(src));

    return true;
  }

  Future<bool> importTownshipsFromCSV_4(List<int> csvData, CsvCodec codec) async {
    return this.importObjectFromCSV_4(csvData, codec, (src) async =>
      await DatabaseHelper().saveTownshipWithId(Township.map(src)));
  }

  Future<bool> exportTownshipsToCSV_4(String fullPath) async {
    var header = [<dynamic>["id", "name"]];
    var arr = header + (await this.getAllTownships()).map((Township t) {
      return [t.id, t.name];
    }).toList();
    return this._streamToCSV(fullPath, arr);
  }

  Future<bool> importUsersFromCSV_4(List<int> csvData, CsvCodec codec) async {
    return this.importObjectFromCSV_4(csvData, codec, (src) async =>
      await DatabaseHelper().saveUserWithId(User.map(src)));
  }

  Future<bool> exportUsersToCSV_4(String fullPath) async {
    var header = [<dynamic>["id", "name", "townshipId", "isDisabled"]];
    var arr = header + (await this.getAllUsers()).map((User u) {
      return [u.id, u.name, u.townshipId, u.disabled];
    }).toList();
    return this._streamToCSV(fullPath, arr);
  }

  Future<bool> importFestivalsFromCSV_4(List<int> csvData, CsvCodec codec) async {
    return this.importObjectFromCSV_4(csvData, codec, (src) async =>
      await DatabaseHelper().saveFestivalWithId(Festival.map(src)));
  }

  Future<bool> exportFestivalsToCSV_4(String fullPath) async {
    var header = [<dynamic>["id", "name", "townshipId", "dateMSSinceEpoch", "trophies", "rubies", "diamonds", "tasks", "distributionMethod"]];
    var arr = header + (await this.getAllFestivals()).map((Festival f) {
      return [f.id, f.name, f.townshipId, f.dateMSSinceEpoch, f.yellow, f.red, f.blue, f.tasks, f.method];
    }).toList();
    return this._streamToCSV(fullPath, arr);
  }

  Future<bool> importScoresFromCSV_4(List<int> csvData, CsvCodec codec) async {
    return this.importObjectFromCSV_4(csvData, codec, (src) async =>
      await DatabaseHelper().saveScoreWithId(Score.map(src)));
  }

  Future<bool> exportScoresToCSV_4(String fullPath) async {
    var header = [<dynamic>["id", "userId", "festivalId", "townshipId", "trophies", "tasks"]];
    var arr = header + (await this.getAllScores()).map((Score s) {
      return [s.id, s.uid, s.fid, s.rid, s.yellow, s.tasks];
    }).toList();
    return this._streamToCSV(fullPath, arr);
  }

  Future<bool> importSelectionsFromCSV_4(List<int> csvData, CsvCodec codec) async {
    return this.importObjectFromCSV_4(csvData, codec, (src) async =>
      await DatabaseHelper().saveSelectionWithId(Selection.map(src)));
  }

  Future<bool> exportSelectionsToCSV_4(String fullPath) async {
    var header = [<dynamic>["id", "townshipId", "festivalId", "scoreId", "flag"]];
    var arr = header + (await this.getAllSelections()).map((Selection s) {
      return [s.id, s.townshipId, s.festivalId, s.scoreId, s.flag];
    }).toList();
    return this._streamToCSV(fullPath, arr);
  }

  Future<bool> writeJsonToFile(String fullPath, dynamic object) async {
    var f = io.File(fullPath);
    var sink = f.openWrite();
    try {
      sink.write(jsonEncode(object));
      return true;
    } catch(e) {
      return false;
    } finally {
      await sink.close();
    }
  }

  Future<bool> _streamToCSV(String fullPath, List<List> arr) async {
    var convert = ListToCsvConverter();
    var f = io.File(fullPath);

    var sink = f.openWrite();
    try {
      sink.write(convert.convert(arr));
    } finally {
      await sink.close();
    }

    return true;
  }

  Future<int> saveTownship(Township r) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_TOWNSHIPS, r.toMapNoId());
    return res;
  }

  Future<int> saveTownshipWithId(Township r) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_TOWNSHIPS, r.toMap());
    return res;
  }

  Future<int> saveUser(User u) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_USERS, u.toMapNoId());
    return res;
  }

  Future<int> saveUserWithId(User u) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_USERS, u.toMap());
    return res;
  }

  Future<int> saveFestivalWithId(Festival f) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_FESTIVALS, f.toMap());
    return res;
  }

  Future<int> saveScoreWithId(Score s) async {
    var dbClient = await db;
    int res = await dbClient.insert(TABLE_SCORE, s.toMap());
    return res;
  }

  Future<int> saveSelectionWithId(Selection s) async {
    var dbClient = await db;
    print("serializing selection to object: ${s.toMap()}");
    int res = await dbClient.insert(TABLE_SELECTION, s.toMap());
    return res;
  }

  Future<int> countTownships() async {
    var dbClient = await db;
    var ret = await dbClient.rawQuery("SELECT count(*) as cnt from $TABLE_TOWNSHIPS");
    return ret.first['cnt'];
  }

  Future<List<Township>> loadTownships() async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_TOWNSHIPS,
      columns: ["id", "name"],
      orderBy: "name",
    );

    return ret.map((row) => Township.map(row)).toList();
  }

  Future<Township> getTownshipById(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_TOWNSHIPS,
      columns: ["id", "name"],
      where: "id=?",
      whereArgs: [id]);

    return Township.map(ret[0]);
  }

  Future<List<Selection>> getAllSelections() async {
    var dbClient = await db;
    return (await dbClient.query(TABLE_SELECTION)).map((r) {
      return Selection.map(r);
    }).toList();
  }

  Future<List<Township>> getAllTownships() async {
    var dbClient = await db;
    return (await dbClient.query(TABLE_TOWNSHIPS)).map((r) {
      return Township.map(r);
    }).toList();
  }

  Future<void> removeAllTownships() async =>
    (await this.getAllTownships()).forEach((t) async =>
        await this.removeTownshipById(t.id));

  Future<List<User>> getAllUsers() async {
    var dbClient = await db;
    return (await dbClient.query(TABLE_USERS)).map((r) {
      return User.map(r);
    }).toList();
  }

  Future<void> removeAllUsers() async =>
      (await this.getAllUsers()).forEach((u) async =>
          await this.removeUserById(u.id));

  Future<List<Festival>> getAllFestivals() async {
    var dbClient = await db;
    return (await dbClient.query(TABLE_FESTIVALS)).map((r) {
      return Festival.map(r);
    }).toList();
  }

  Future<List<Score>> getAllScores() async {
    var dbClient = await db;
    return (await dbClient.query(TABLE_SCORE)).map((r) {
      return Score.map(r);
    }).toList();
  }

  Future<List<User>> loadActiveUsersForTownshipId(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_USERS,
      columns: ["id", "rid", "name", "disabled"],
      orderBy: "name",
      where: "disabled=0 and rid=?",
      whereArgs: [id],
    );

    return ret.map((row) => User.map(row)).toList();
  }

  Future<List<int>> loadUsersForFestivalId(int id) async {
    var dbc = await db;
    var ret = await dbc.rawQuery("select distinct(uid) as uid from $TABLE_SCORE where fid=?",
      [id]);

    var lst = List<int>();
    for(var row in ret) {
      lst.add(row['uid']);
    }

    return lst;
  }

  Future<Map<int, bool>> loadSelectionMapForFestivalId(int fid) async {
    var dbc = await db;
    var ret = await dbc.query(TABLE_SELECTION,
      columns: ["sid", "flag"],
      where: "fid=?",
      whereArgs: [fid]);

    var map = Map<int, bool>();
    for(var row in ret) {
      map[row["sid"]] = row["flag"] == 1;
    }

    return map;
  }

  Future<List<User>> loadAllUsersForTownshipId(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_USERS,
      columns: ["id", "rid", "name", "disabled"],
      orderBy: "name",
      where: "rid=?",
      whereArgs: [id],
    );

    return ret.map((row) => User.map(row)).toList();
  }

  Future<List<Festival>> loadFestivalsForTownshipId(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_FESTIVALS,
      columns: ["id", "rid", "name", "date", "yellow", "red", "blue", "tasks", "method"],
      orderBy: "date desc",
      where: "rid=?",
      whereArgs: [id]);

    return ret.map((row) => Festival.map(row)).toList();
  }

  Future<List<User>> loadActiveUsersForTownship(Township r) async =>
    this.loadActiveUsersForTownshipId(r.id);

  Future<int> addFestival(Festival f) async =>
    (await db).insert(TABLE_FESTIVALS, f.toMapNoId());

  Future<int> addFestivalRetainingId(Festival f) async =>
    (await db).insert(TABLE_FESTIVALS, f.toMap());

  Future<void> rawRemoveUsersByTownshipId(int id) async =>
    (await db).delete(TABLE_USERS, where: "rid=?", whereArgs: [id]);

  Future<void> rawRemoveFestivalsByTownshipId(int id) async =>
    (await db).delete(TABLE_FESTIVALS, where: "rid=?", whereArgs: [id]);

  Future<void> rawRemoveFestivalById(int id) async =>
    (await db).delete(TABLE_FESTIVALS, where: "id=?", whereArgs: [id]);

  Future<void> rawRemoveTownshipById(int id) async =>
    (await db).delete(TABLE_TOWNSHIPS, where: "id=?", whereArgs: [id]);

  Future<void> rawRemoveScoresByFestivalId(int id) async =>
    (await db).delete(TABLE_SCORE, where: "fid=?", whereArgs: [id]);

  Future<void> rawRemoveScoresByTownshipId(int id) async =>
    (await db).delete(TABLE_SCORE, where: "rid=?", whereArgs: [id]);

  Future<void> rawRemoveSelectionsByFestivalId(int fid) async =>
    (await db).delete(TABLE_SELECTION, where: "fid=?", whereArgs: [fid]);

  Future<void> rawRemoveSelectionsByTownshipId(int rid) async =>
    (await db).delete(TABLE_SELECTION, where: "rid=?", whereArgs: [rid]);

  Future<void> removeFestivalById(int id) async {
    await this.rawRemoveFestivalById(id);
    await this.rawRemoveScoresByFestivalId(id);
    await this.rawRemoveSelectionsByFestivalId(id);
    return 0;
  }

  Future<void> removeTownshipById(int id) async {
    await this.rawRemoveFestivalsByTownshipId(id);
    await this.rawRemoveScoresByTownshipId(id);
    await this.rawRemoveUsersByTownshipId(id);
    await this.rawRemoveTownshipById(id);
    await this.rawRemoveSelectionsByTownshipId(id);
    return 0;
  }

  Future<void> removeFestival(Festival f) async =>
    this.removeFestivalById(f.id);

  Future<Festival> loadFestivalById(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_FESTIVALS,
      columns: ["id", "rid", "name", "date", "yellow", "red", "blue", "tasks", "method"],
      orderBy: "date desc",
      where: "id=?",
      whereArgs: [id]);

    return ret.map((row) => Festival.map(row)).toList()[0];
  }

  Future<Festival> getFestivalById(int id) async =>
    this.loadFestivalById(id);

  Future<void> removeUserById(int userId) async {
    var dbClient = await db;
    await dbClient.delete(TABLE_USERS,
      where: "id=?",
      whereArgs: [userId]);

    return 0;
  }

  Future<void> disableUserById(int userId) async {
    var dbClient = await db;
    await dbClient.update(TABLE_USERS, {"disabled": 1},
      where: "id=?",
      whereArgs: [userId]);

    return 0;
  }

  Future<void> removeScoreById(int scoreId) async {
    var dbClient = await db;
    await dbClient.delete(TABLE_SCORE,
      where: "id=?",
      whereArgs: [scoreId]);

    await dbClient.delete(TABLE_SELECTION,
      where: "sid=?",
      whereArgs: [scoreId]);

    return 0;
  }

  Future<bool> renameUserById(int uid, String newName) async {
    var dbc = await db;
    var changes = await dbc.update(TABLE_USERS, {"name": newName}, where: "id=?", whereArgs: [uid]);
    return changes > 0;
  }

  Future<User> getUserById(int id) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_USERS,
      columns: ["id", "rid", "name"],
      where: "id=?",
      whereArgs: [id]);

    return User.map(ret[0]);
  }

  Future<int> countUsersForTownshipId(int rid) async {
    var dbClient = await db;
    var ret = await dbClient.rawQuery(
      "select count(*) as c from $TABLE_USERS where disabled=0 and rid=?",
      [rid]);

    return ret[0]["c"];
  }

  Future<void> clearDatabase() async {
    var dbPath = await getDatabasesPath();
    await io.File(join(dbPath, DB_FILENAME)).delete();
    await io.File(join(dbPath, "$DB_FILENAME-wal")).delete();
    await io.File(join(dbPath, "$DB_FILENAME-shm")).delete();
    return;
  }

  Future<int> addScore(Score s) async {
    var dbClient = await db;
    var scoreId = await dbClient.insert(TABLE_SCORE, s.toMapNoId());
    return scoreId;
  }

  Future<Score> getScoreById(int scoreId) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_SCORE,
      columns: ["id", "uid", "fid", "rid", "yellow", "tasks"],
      where: "id=?",
      whereArgs: [scoreId]);

    return Score.map(ret[0]);
  }

  Future<int> getScoreSumForUserId(int userId) async {
    var dbc = await db;
    var ret = await dbc.rawQuery(
      "select sum(yellow) as c from $TABLE_SCORE where uid=?",
      [userId]);

    return ret[0]["c"];
  }

  Future<List<Score>> getScoreListByFestivalId(int fid) async {
    var dbClient = await db;
    var ret = await dbClient.query(TABLE_SCORE,
      columns: ["id", "uid", "fid", "yellow", "tasks"],
      where: "fid=?",
      whereArgs: [fid]);

    var lst = ret.map((row) => Score.map(row)).toList();
    lst.sort((a, b) => b.yellow - a.yellow);
    return lst;
  }

  Future<int> countNumberOfFestivalsForUserId(int uid) async {
    var dbc = await db;
    var ret = await dbc.rawQuery(
      "select count(distinct(fid)) as c from $TABLE_SCORE where uid=?",
      [uid]);

    return ret[0]["c"];
  }

  Future<bool> isPlayerSelectedByScoreId(int sid) async {
    var dbc = await db;
    var ret = await dbc.rawQuery(
      "select flag from $TABLE_SELECTION where sid=?",
      [sid]);

    return ret[0]["flag"] == 1 ? true : false;
  }

  Future<void> setPlayerSelectionByScoreId(int rid, int fid, int sid, bool selected) async {
    var dbc = await db;
    var ret = await dbc.rawQuery("SELECT count(*) as cnt "
      "from $TABLE_SELECTION where sid=?", [sid]);

    if(ret[0]["cnt"] > 0) {
      if(selected) {
        await dbc.update(TABLE_SELECTION, { "flag": 1 },
          where: "sid=?",
          whereArgs: [sid]);
      } else {
        await dbc.delete(TABLE_SELECTION,
          where: "sid=?",
          whereArgs: [sid]);
      }
    } else {
      await dbc.insert(TABLE_SELECTION, {
        "sid": sid,
        "rid": rid,
        "fid": fid,
        "flag": selected ? 1 : 0,
      });
    }
  }

  Future<void> setFestivalMethod(int fid, int method) async {
    var dbc = await db;
    await dbc.update(TABLE_FESTIVALS, {"method": method},
      where: "id=?",
      whereArgs: [fid]);

    return;
  }

  Future<int> getFestivalMethod(int fid) async {
    var dbc = await db;
    var ret = await dbc.query(TABLE_FESTIVALS,
        columns: ["method"],
        where: "id=?",
        whereArgs: [fid]);

    return ret[0]["method"];
  }

  Future<void> deleteAllConfigs() async {
    var dbc = await db;
    await dbc.rawQuery("delete from $TABLE_METADATA");
  }

  Future<Result<String>> getConfigTextById(int configId) async {
    var dbc = await db;
    var ret = await dbc.query(TABLE_METADATA,
      columns: ["confid", "conftext"],
      where: "confid=?",
      whereArgs: [configId]);

    if(ret != null && ret.length > 0) {
      return Result.value(ret[0]["conftext"]);
    } else {
      return Result.error("No such config");
    }
  }

  Future<bool> doesConfigIdExists(int configId) async {
    var dbc = await this.db;
    var ret = await dbc.rawQuery("SELECT count(*) as cnt from $TABLE_METADATA where confid=$configId");
    return ret.first['cnt'] > 0;
  }

  Future<int> updateExistingConfigId(int configId, String configText) async {
    var dbc = await this.db;
    return dbc.update(TABLE_METADATA, {
      "conftext": configText
    }, where: "confid=?", whereArgs: [configId]);
  }

  Future<int> createNewConfigId(int configId, String configText) async {
    var dbc = await this.db;
    return dbc.insert(TABLE_METADATA, {
      "confid": configId,
      "conftext": configText,
    });
  }

  Future<bool> setConfigTextById(int configId, String configText) async {
    try {
      // lock db
      if(await this.doesConfigIdExists(configId)) {
        return 1 == await this.updateExistingConfigId(configId, configText);
      } else {
        return 1 == await this.createNewConfigId(configId, configText);
      }
    } finally {
      // unlock db
    }
  }
}
