/*import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'chart_widget.dart';

class ChartPage extends StatefulWidget {
  ChartPage({@required this.chartWidget});
  final ChartWidget chartWidget;

  @override
  _ChartPageState createState() => _ChartPageState();
}

class _ChartPageState extends State<ChartPage> {
  List<ChartData> data;

  @override
  void initState() {
    super.initState();
    this._loadChartData();
  }

  void _loadChartData() {
    this.widget.chartWidget.loadData().then((dataList) {
      setState(() {
        this.data = dataList;
      });
    });
  }

  List<charts.Series<ChartData, String>> _createSeries() {
    return [
      charts.Series<ChartData, String>(
        id: "chart",
        domainFn: (cd, _) => cd.name,
        measureFn: (cd, _) => cd.points,
        data: this.data,
      )
    ];
  }

  Widget _chartFrame(BuildContext ctx, Widget w) {
    var subtitle = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(padding: EdgeInsets.fromLTRB(12, 12, 12, 12), child:
          Text(this.widget.chartWidget.subtitle,
            textAlign: TextAlign.center,
            style: Theme.of(ctx).primaryTextTheme.subtitle)),
        ]);

    var col = Column(children: [
      Expanded(child: Padding(padding: EdgeInsets.fromLTRB(12, 12, 12, 12), child: w)),
      subtitle,
    ]);
    return col;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.chartWidget.title)
      ),
      body: this.data == null ? Text("") : _chartFrame(context, charts.BarChart(
        _createSeries(),
        animate: true,
        vertical: false,
      ))
    );
  }
}*/
