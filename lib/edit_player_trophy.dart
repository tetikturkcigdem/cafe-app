import 'package:flutter/cupertino.dart';
import 'package:cafe_app/user.dart';
import 'package:cafe_app/image_box.dart';

class EditPlayerTrophy extends StatefulWidget {
  EditPlayerTrophy({Key key, this.user, @required this.maxTrophies}) : super(key: key);

  final User user;
  final int maxTrophies;

  @override
  _EditPlayerTrophyState createState() => _EditPlayerTrophyState();
}

class _EditPlayerTrophyState extends State<EditPlayerTrophy> {
  int _points = 0;

  //Widget _trophyPointsForm(BuildContext ctx) {
    /*
    var f = Form(
      key: _formKey,
      child: TextFormField(
        autofocus: false,
        style: TextStyle(fontSize: 24.0, color: Colors.black),
        keyboardType: TextInputType.numberWithOptions(),
        decoration: InputDecoration(
          labelText: "0 - ${widget.maxTrophies}",
          border: OutlineInputBorder(),
        ),
        onSaved: (s) {
          _points = int.parse(s);
        },
        validator: (s) {
          var errMsg = "Enter a value greater than 0 and smaller than ${widget.maxTrophies + 1}";
          if(s == null || s.trim().length == 0)
            return errMsg;

          int n = int.parse(s);
          if (n == 0 || n == null || n > widget.maxTrophies)
            return errMsg;
        },
      ));

    return Padding(padding: const EdgeInsets.only(top: 18.0), child: f);
    */

    //return Text("placeholder");
  //}

  void _addPlayer() {
    Navigator.of(this.context).pop(_points);
  }

  Widget _buildBody(BuildContext ctx) {
    var container = Container(
        child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Center(
                child: Column(children: [
                  ImageBox(
                    imagePath: "assets/trophy.png",
                    width: 150.0,
                    height: 150.0,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(top: 12.0, bottom: 18.0),
                      child: Text(widget.user.name, textScaleFactor: 2.5)),
                  Text("Trophy points of this player?"),
                  //_trophyPointsForm(ctx),
                ])
            )
        )
    );

    var children = <Widget>[container];
    var _lv = ListView(children: children);
    return _lv;
  }

  @override
  Widget build(BuildContext context) {
    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Text("OK"),
      onPressed: () => _addPlayer,
    );

    var _navBar = CupertinoNavigationBar(
      middle: Text("Set trophy points"),
      previousPageTitle: "Add player",
      trailing: _addButton,
    );

    return CupertinoPageScaffold(
        navigationBar: _navBar,
        child: this._buildBody(context));
  }
}
