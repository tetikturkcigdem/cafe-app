import 'package:cafe_app/add_township_page.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter/cupertino.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/fullscreen_message_page.dart';
import 'package:cafe_app/database_helper.dart';
import 'package:cafe_app/township.dart';
import 'package:cafe_app/township_list.dart';
import 'package:cafe_app/loading_page.dart';
import 'package:cafe_app/debug_utils.dart' as DebugUtils;
import 'package:cafe_app/global_config.dart';
import 'package:cafe_app/options_page.dart';
import 'package:cafe_app/ads.dart' as Ads;

class MyTownshipsPage extends StatefulWidget {
  MyTownshipsPage({Key key, @required this.darkModeChanged}): super(key: key);

  DarkModeChangedCallback darkModeChanged;

  @override
  _MyTownshipsPageState createState() => new _MyTownshipsPageState();
}

class _MyTownshipsPageState extends State<MyTownshipsPage> {
  List<Township> _restaurantList;
  bool _loading = true;
  //bool _debugMode = false;
  bool _firstRunCompleted = false;

  @override void initState() {
    super.initState();

    DebugUtils.run(() {
      //setState(() => this._debugMode = true);
      //DatabaseHelper().exportToCSV();
    });

    _firstRun().then((_) => setState(() {
      this._firstRunCompleted = true;
    }));

    this._refreshRestaurantList();
  }

  Future<Null> _firstRun() async {
    // Nothing at the moment!
  }

  void _refreshRestaurantList() {
    setState(() { _loading = true; });

    print("Refreshing township list");

    DatabaseHelper().loadTownships().then((List<Township> restaurantList) {
      setState(() {
        _restaurantList = restaurantList;
        _loading = false;
      });
    });
  }

  void _showAddRestaurantPage(BuildContext context) {
    Navigator.push(context, CupertinoPageRoute(
      builder: (context) => AddTownshipPage()
    )).then((result) {
      // `result` should be 'int 1'
      if(result != null) {
        _refreshRestaurantList();
      } else {
        // Nothing.
      }
    });
  }

  void _showOptionsPage(BuildContext ctx) {
    Navigator.push(context, CupertinoPageRoute(
      builder: (context) => OptionsPage(widget.darkModeChanged),
    )).then((_) {
      this._refreshRestaurantList();
    });
  }

  Widget _buildLoadedBody(BuildContext context) {
    int cnt = _restaurantList.length;

    if(cnt == 0) {
      return FullscreenMessagePage(message: i18n(this.context).townNothingAddedYet);
    } else {
      return TownshipList(items: _restaurantList,
        onBack: () {
          this._refreshRestaurantList();
        });
    }
  }

  Widget buildBody(BuildContext context) {
    return _buildLoadedBody(context);
  }

  @override Widget build(BuildContext context) {
    Locale loc = Localizations.localeOf(context);
    String locStr = "${loc.languageCode}_${loc.countryCode}";
    initializeDateFormatting(locStr);

    var _optionsButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Icon(CupertinoIcons.gear_big),
      onPressed: () => _showOptionsPage(context));

    var _addButton = CupertinoButton(
      padding: EdgeInsets.all(0),
      child: Icon(CupertinoIcons.add),
      onPressed: () => _showAddRestaurantPage(context));

    var _navigationBar = CupertinoNavigationBar(
      middle: Text(i18n(context).townList),
      leading: _optionsButton,
      trailing: _addButton,
    );

    if(this._loading || this._firstRunCompleted != true) {
      return LoadingPage();
    } else {
      return CupertinoPageScaffold(
        navigationBar: _navigationBar,
        child: buildBody(context),
      );
    }
  }
}
