import 'dart:async';

import 'package:cafe_app/cupertino_strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_crashlytics/flutter_crashlytics.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import 'package:cafe_app/global_config.dart';
import 'package:cafe_app/my_townships_page.dart';
import 'package:cafe_app/strings.dart';
import 'package:cafe_app/ads.dart' as Ads;
import 'package:cafe_app/debug_utils.dart' as DebugUtils;

void main() async {
  FlutterError.onError = (FlutterErrorDetails details) {
    print("onError");
    DebugUtils.invoke<void>(debugCode: () {
      // In development mode simply print to console.
      print("--- Exception detected in Debug mode! ---");
      FlutterError.dumpErrorToConsole(details);
    }, releaseCode: () {
      // In production mode report to the application zone to report to
      // Crashlytics.
      print("--- Exception detected in Release mode ---");
      Zone.current.handleUncaughtError(details.exception, details.stack);
    });
  };

  WidgetsFlutterBinding.ensureInitialized();
  await FlutterCrashlytics().initialize();

  runZoned<Future<Null>>(() async {
    runMainInOwnZone();
  }, onError: (error, stackTrace) async {
    if(stackTrace != null) {
      print(stackTrace);
    } else {
      print("'stackTrace' is null, can't dump to console");
    }

    // Whenever an error occurs, call the `reportCrash` function. This will send
    // Dart errors to our dev console or Crashlytics depending on the environment.
    await FlutterCrashlytics().reportCrash(error, stackTrace, forceCrash: false);
  });
}

void runMainInOwnZone() {
  print("Starting MyCafeCalculator app");
  Ads.initNative();
  GlobalConfig.initAsync().then((GlobalConfig c) {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  MyApp();

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool showAd = true;
  CupertinoThemeData currentTheme;

  @override
  void initState() {
    super.initState();
    this.initTheme();
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          this.showAd = !visible;
        });
      }
    );
  }

  void darkModeChanged(bool flag) {
    print("Dark mode flag: " + flag.toString());
    setState(() {
      GlobalConfig.get().enableDarkMode = flag;
      this.initTheme();
    });
  }

  void initTheme() {
    bool darkMode = GlobalConfig.get().enableDarkMode;
    this.currentTheme = new CupertinoThemeData(
      brightness: darkMode ? Brightness.dark : Brightness.light);
  }

  @override
  Widget build(BuildContext context) {
    var app = CupertinoApp(
      localizationsDelegates: [
        StringLocalizationsDelegate(),
        LocalCupertinoLocalizationsDelegate(),
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        // Default locale, leave EN on the beginning!
        const Locale("en", ""),
        // ----
        // Optional locales:
        const Locale("pl", ""),
        const Locale("es", ""),
        const Locale("pt", "br"),
      ],
      title: 'CafeApp',
      theme: currentTheme,
      home: MyTownshipsPage(
        darkModeChanged: darkModeChanged),
      builder: (BuildContext context, Widget widget) {
        return Ads.appContainer(showAd, context, widget);
      },
      navigatorObservers: [
        // ...
      ]
    );
    return app;
  }
}
